<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="menu_tree_accordion" class="easyui-accordion" data-options="fit:false,border:false">
	<div title="权限设置" data-options="fit:false,border:false">
		<ul class="easyui-tree menu-tree">
			<li>
				<span>
					<a href="#" onclick="addTab('userController','用户维护','userController/init');">用户维护</a>
				</span>
			</li>
			<li>
				<span>
					<a href="#" onclick="addTab('orgController','组织维护','orgController/init');">组织维护</a>
				</span>
			</li>
			<li>
				<span>
					<a href="#" onclick="addTab('roleController','角色维护','roleController/init');">角色维护</a>
				</span>
			</li>
			<li>
				<span>
					<a href="#" onclick="addTab('functionController','功能维护','functionController/init');">功能维护</a>
				</span>
			</li>
		</ul>
	</div>
	<div title="系统设置" data-options="fit:false,border:false">
		<ul class="easyui-tree menu-tree">
			<li>
				<span>
					<a href="#" onclick="addTab('serviceParamController','环境变量设定','jsp/llsfw/serverParam/serverParam.jsp');">环境变量设定</a>
				</span>
			</li>
			<li>
				<span>
					<a href="#" onclick="addTab('quartzController','定时任务管理','quartzController/init');">定时任务管理</a>
				</span>
			</li>
		</ul>
	</div>
</div>

